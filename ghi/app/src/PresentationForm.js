import React from 'react'

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenter_name: '',
            presenter_email: '',
            company_name: '',
            title: '',
            synopsis: '',
            conference: '',
            conferences: []
        }

        this.handleChangeName = this.handleChangeName.bind(this)
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangeTitle = this.handleChangeTitle.bind(this)
        this.handleChangeSynopsis = this.handleChangeSynopsis.bind(this)
        this.handleChangeConference = this.handleChangeConference.bind(this)
        this.handleChangeCompany = this.handleChangeCompany.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChangeName(event) {
        const value = event.target.value
        this.setState({ presenter_name: value })
    }

    handleChangeEmail(event) {
        const value = event.target.value
        this.setState({ presenter_email: value })
    }

    handleChangeTitle(event) {
        const value = event.target.value
        this.setState({ title: value })
    }

    handleChangeSynopsis(event) {
        const value = event.target.value
        this.setState({ synopsis: value })
    }

    handleChangeConference(event) {
        const value = event.target.value
        this.setState({ conference: value })
    }

    handleChangeCompany(event) {
        const value = event.target.value
        this.setState({ company_name: value })
    }

    async componentDidMount() {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log("conferences::", data)
            this.setState({ conferences: data.conferences });
        }
    }

    async handleSubmit(event) {
        event.preventDefault()
        const data = { ...this.state };
        delete data.conferences;
        const conferenceId = data.conference;
        console.log('data::', data)


        const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {

            this.setState({
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            })
        }
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={this.handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" onChange={this.handleChangeName} value = {this.state.presenter_name}/>
                                <label htmlFor="presenter_name">Presenter name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" onChange={this.handleChangeEmail} value = {this.state.presenter_email} />
                                <label htmlFor="presenter_email">Presenter email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" onChange={this.handleChangeCompany} value = {this.state.company_name}/>
                                <label htmlFor="company_name">Company name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input placeholder="Title" required type="text" name="title" id="title" className="form-control" onChange={this.handleChangeTitle} value = {this.state.title}/>
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea className="form-control" id="synopsis" rows="3" name="synopsis" onChange={this.handleChangeSynopsis} value = {this.state.synopsis}></textarea>
                            </div>
                            <div className="mb-3">
                                <select required name="conference" id="conference" className="form-select" onChange={this.handleChangeConference} value = {this.state.conference}>
                                    <option value="">Choose a conference</option>
                                    {this.state.conferences.map(conference => {
                                        return (
                                            <option key={conference.id} value={conference.id}>
                                                {conference.name} </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default PresentationForm
